/*

  @file     sptlzr_pots_test.ino
  @purpose  Spatializer module potentiometers test
  @links    https://www.arduino.cc/en/tutorial/pushbutton
  @notes
    Potentiometers :
      + 3 analog potentiometer : arduino pins A0, A1, A2
      + 1 push button : arduino pin 2 (digital) 
      + 1 control voltage input : analog pin A3

*/


/**
 *
 * INCLUDE
 *
 */
// SPI library:
#include <SPI.h>



/**
 *
 * GLOBAL VARIABLES
 *
 */
// Serial console messages
boolean serial_on = true;
long serial_rate = 9600; 
boolean serial_dbug = true;

// Digital pots
int dpot_pb_pin = 2;

// Analog pots
int apot_1_pin = A0;
int apot_2_pin = A1;
int apot_3_pin = A2;

// trigger input (cv in)
int cvin_pin = A3;



/**
 *
 * SETUP
 *
 */
void setup() {

  //
  if (serial_on) Serial.begin(serial_rate);
  // push button
  pinMode(dpot_pb_pin, INPUT);
  digitalWrite(dpot_pb_pin, HIGH); // pull-up

}



/**
 *
 * LOOP
 *
 */
void loop() {
  //
  Serial.println("$$$  Pots values  $$$");
  //
  Serial.print("analog pots: 1= ");
  Serial.print(readAnalogPot(apot_1_pin));
  Serial.print(", 2= "); 
  Serial.print(readAnalogPot(apot_2_pin));
  Serial.print(", 3= "); 
  Serial.print(readAnalogPot(apot_3_pin));
  //
  Serial.print(" | push_button : "); 
  int push_button_state = readPushButtonState();
  Serial.print(push_button_state ? "DOWN" : "UP");
  //
  Serial.print(" | CV IN = "); 
  Serial.println(readAnalogPot(cvin_pin));
    

  // delay sequence speed
  delay(100); 
}



/**
 *
 * GENERAL FUNCTIONS
 *
 */
//
int readAnalogPot(int pin) {
  return analogRead(pin);
}


//
int readPushButtonState() {
  int button_pressed = !digitalRead(dpot_pb_pin); // pin low -> pressed 
  return button_pressed;
}


