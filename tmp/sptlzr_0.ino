/*

  @file     tb3337.ino
  @purpose  Arduino audio spatializer module
  @links    https://www.arduino.cc/en/Tutorial/DigitalPotControl       
            http://auto.polytech.univ-tours.fr/telechargements/fichiers/SA.poly.pdf 
  @notes  
  Controls :
  + 1 digital potentiometer [audio volumes for each channel/speaker]
  + 1 analog potentiometer [sequence switch]
  + 1 analog potentiometer [sequence speed]
  ? 1 analog potentiometer [signal shape: linear, sin, gaussian?, etc..]
  ? 1 analog input [trigger spatialization effect on audio input]

  @todo
  - add signal shape functions (lin, sin, gauss, etc..)
  - add trigger input interactions
  - add colliding sound walls and rotating sound slice effects
 
*/

/**
 *
 * INCLUDE
 *
 */
// SPI library:
#include <SPI.h>


/**
 *
 * GLOBAL VARIABLES
 *
 */
// Serial console messages
boolean serial_on = true;
long serial_rate = 9600; 
boolean serial_dbug = true;

// set pin 10 as the slave select for the digital pot:
const int slaveSelectPin = 10;

//la vitesse, ou plus exactement le délai entre chaque pas de séquence.
//Cette valeur est lue sur le input analogue A0 dans la fonction readSpeed. 
unsigned char myspeed=10;

int main_speakers[2] = {0, 1};

int n_speakers = 6;
int* seq;
int seq1[6] = {0,1,2,3,4,5}; 
int seq2[6] = {0,5,4,3,2,1};
int seq3[6] = {0,3,5,1,4,2};

bool seq_on = false;

int seq_size = 6;
int seq_step = 0;

int sp_vol[6] = {0,0,0,0,0,0};   

int curr_sp_idx = 0;
int curr_seq_idx = 0;
int next_sp_idx = 0;
int next_seq_idx = 0;

int curr_step_idx = 0;
int n_steps = 8;  // peut etre modifie par un potard aussi
int vol_step = 256/n_steps;
const int vol_max = 255;



/**
 *
 * SETUP
 *
 */
void setup() {
  // set the slaveSelectPin as an output:
  Serial.begin(9600);
  pinMode(slaveSelectPin, OUTPUT);
  // initialize SPI
  SPI.begin();

  // 
  seq = (int *)seq1;
  curr_seq_idx = 0;
  next_seq_idx = 1;
  next_sp_idx = seq[next_seq_idx];
}



/**
 *
 * LOOP
 *
 */
void loop() {

  // check if sequence was changed (analog pot A2)
  checkSequenceSwitch();
   
  // update current sequence
  updateSeq(); 

  // new speakers volume calculation
  updateSpeakersVolume();

  // send speakers volume to digital pot 
  for (int channel = 0; channel < 6; channel++) { 
    digitalPotWrite(channel,sp_vol[channel]);
  }

  // dbug
  if (serial_dbug) {
    printSpeakersVolume(); 
    //printIdx();
  }

  // delay sequence speed
  delay(readSpeed()); 
}



/**
 *
 * GENERAL FUNCTIONS
 *
 */
void checkSequenceSwitch(){
  int sensorValue2 = analogRead(A2);
  //Serial.println(sensorValue2);

  seq_on = true;
  if (sensorValue2 >= 256 && sensorValue2 < 512) seq=(int*)seq1;
  else if (sensorValue2 >= 512 && sensorValue2 < 768) seq=(int*)seq2;
  else if (sensorValue2 >= 768) seq=(int*)seq3;
  else seq_on = false;
}


void updateSeq() {
  //
  if (curr_step_idx < n_steps - 1) {
    curr_step_idx++;     
  } else {
    curr_step_idx = 0;
    incrSeqIdx(); 
  }
  //
  curr_sp_idx = seq[curr_seq_idx];
  next_sp_idx = seq[next_seq_idx]; 
  
}


void incrSeqIdx() { 
  //
  if (next_seq_idx == seq_size - 1) {
    next_seq_idx = 0;
    curr_seq_idx = seq_size - 1; 
  } else {
    //Serial.println("YOOOOOOOOOOOOOOOOOOOO");
    next_seq_idx++;
    curr_seq_idx++; 
  }
}



void updateSpeakersVolume() {
  if (seq_on) {
    for (int idx = 0; idx < n_speakers; idx++) { 
      if (idx == curr_sp_idx) {
        sp_vol[idx] = (sp_vol[idx] - vol_step >= 0) ? sp_vol[idx] - vol_step : 0;  
      } else if (idx == next_sp_idx) {
        sp_vol[idx] = (sp_vol[idx] + vol_step < vol_max) ? sp_vol[idx] + vol_step : vol_max;  
      } else {
        sp_vol[idx]= 0;
      }
    }
  } else {
    sp_vol[0] = 230;
    sp_vol[1] = 230;
    sp_vol[2] = 0;
    sp_vol[3] = 0;
    sp_vol[4] = 0;
    sp_vol[5] = 0;
  } 
}



/*
 *  
 *  FUNCTIONS
 * 
 */
int readSpeed(){
  int sensorValue = analogRead(A0)/10 +1;
  if (myspeed/4 != sensorValue/4) {
    myspeed = sensorValue;
  }
  return sensorValue;
}


// 
void digitalPotWrite(int address, int value) {
  // take the SS pin low to select the chip:
  digitalWrite(slaveSelectPin, LOW);
  //  send in the address and value via SPI:
  SPI.transfer(address);
  SPI.transfer(value);
  // take the SS pin high to de-select the chip:
  digitalWrite(slaveSelectPin, HIGH);
}


//
void printSpeakersVolume() { 
  //
  if (serial_on) {
    Serial.println("$ Speakers volumes:"); 
    for (int i = 0; i < n_speakers; i++) { 
      Serial.print(sp_vol[i]);
      Serial.print(", ");
    }
    Serial.println("");
  }
}


//
void printIdx() { 
  //
  if (serial_on) {
    Serial.print(curr_seq_idx);
    Serial.print(", ");
    Serial.print(curr_sp_idx);
    Serial.print(", ");  
    Serial.print(next_seq_idx);
    Serial.print(", ");
    Serial.println(next_sp_idx); 
  }
}