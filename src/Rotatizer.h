/*
  @file Rotatizer.h
  
 
 */

#ifndef ROTATIZER_H
#define ROTATIZER_H

#define N_SPEAKERS 6

#include <stdint.h>


//
class Rotatizer {

 //
 public:
  
  Rotatizer(); 

  
  void ;

    
 //
 private:
  
  // speakers 
  uint8_t _sp_vol[N_SPEAKERS];
  uint8_t _sp_idx_curr;
  uint8_t _sp_idx_next;

  // rotation sequence
  uint8_t _seq_step_curr;
  uint8_t _seq_step_next;
  uint8_t _seq_substep;
  boolean _rotate_clockwise;



};

#endif // ROTATIZER.H
